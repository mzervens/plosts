enum OrderIndx
{
	TBEF = 0,
	MBEF = 1,
	DBEF = 2,
	EBEF = 3,
	PBEF = 4,
	ZBEF = 5,
	TAFT = 6,
	MAFT = 7,
	DAFT = 8,
	EAFT = 9,
	PAFT = 10,
	ZAFT = 11,
};