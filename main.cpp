#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include "Order.cpp"
#include "OrderIndx.cpp"
#include "Used.cpp"
#include "UsedIndx.cpp"
#include "Connection.cpp"
#include <stack>
using namespace std;

class VertexGenerator
{
public:
	int symbolLen;
	string * symbols;

	vector<pair<string, Order>> vertex;
	vector<vector<Connection *>> edges;
	vector<int> visitedVertices;


	// Stores the row position for gieven vertex
	// -1 unknown
	// 0 left
	// 1 right
	vector<int> vertexRowPosition;

	vector<stack<int>> paths;

	string startNode;
	string endNode;

	vector<UsedIndx> uIndx;

	VertexGenerator(string arr[], int len)
	{
		symbolLen = len;
		symbols = new string[len];

		for (int i = 0; i < len; i++)
		{
			symbols[i] = arr[i];
		}

		uIndx.push_back(UsedIndx::T);
		uIndx.push_back(UsedIndx::M);
		uIndx.push_back(UsedIndx::D);
		uIndx.push_back(UsedIndx::D);
		uIndx.push_back(UsedIndx::E);
		uIndx.push_back(UsedIndx::E);
		uIndx.push_back(UsedIndx::P);
		uIndx.push_back(UsedIndx::Z);
		uIndx.push_back(UsedIndx::R);

		startNode = "TMDDEEPZ|";
		endNode = "|TMDDEEPZ";

	}
	~VertexGenerator()
	{
		//		delete symbols;
	}

	void generatePermutations(string v, Used u)
	{
		if (v.length() > symbolLen)
		{
			return;
		}

		if (overused(u))
		{
			return;
		}

		if (v.length() == symbolLen)
		{
			Order o = makeOrder(v);

			if (checkDupesVal(v, o))
			{
				vertex.push_back(make_pair(v, o));
			}
			return;
		}

		for (int i = 0; i < symbolLen; i++)
		{
			UsedIndx indx = uIndx[i];
			Used us = u;
			int allowed = 1;
			us.used[indx] += 1;
			if (symbols[i].compare("D") == 0 || symbols[i].compare("E") == 0)
			{
				allowed = 2;
			}

			if (us.used[indx] > allowed)
			{
				continue;
			}

			generatePermutations(v + symbols[i], us);

		}

	}


	void dfs2()
	{
		stack<int> path;
		stack<int> st;

		// Adding additional vertices that will represend same vertices but with the row on the 
		// other side
		int verSize = vertex.size() - 1;

		for (int i = 1; i < verSize; i++)
		{
			vertex.push_back(vertex[i]);
		}

		vector<vector<int>> visitedPaths(vertex.size(), vector<int>(vertex.size()));
		visitedVertices.resize(vertex.size());
		vertexRowPosition.resize(vertex.size());
		for (int i = 0; i < visitedVertices.size(); i++)
		{
			visitedVertices[i] = 0;
			for (int n = 0; n < vertex.size(); n++)
			{
				visitedPaths[i][n] = 0;
			}
		}

		int startVertex;
		int endVertex;
		int currVertex;
		bool remove = true;

		for (int i = 0; i < vertex.size(); i++)
		{
			if (vertex[i].first.compare(startNode) == 0)
			{
				startVertex = i;
				vertexRowPosition[i] = 0;
			}
			else if (vertex[i].first.compare(endNode) == 0)
			{
				endVertex = i;
				vertexRowPosition[i] = 1;
			}
		}


		for (int i = 0; i < vertex.size(); i++)
		{
			if (i != startVertex && i != endVertex)
			{
				if (i < endVertex)
				{
					vertexRowPosition[i] = 0;
				}
				else
				{
					vertexRowPosition[i] = 1;
				}
			}
		}


		currVertex = startVertex;
		st.push(currVertex);
		visitedVertices[currVertex] = 1;


		while (true)
		{

			for (int i = 0; i < vertex.size(); i++)
			{
				if (i == currVertex || visitedVertices[i] == 1 || vertexRowPosition[i] == vertexRowPosition[currVertex] ||
					visitedPaths[currVertex][i] == 1)
				{
					continue;
				}

				if (vertexRowPosition[currVertex] == 0)
				{
					if (isTraversableFromLeftToRight(currVertex, i))
					{
						visitedVertices[i] = 1;
					//	vertexRowPosition[i] = 1;
						visitedPaths[currVertex][i] = 1;
						st.push(i);
						remove = false;
						break;
					}

				}
				else
				{
					if (isTraversableFromRightToLeft(currVertex, i))
					{
						visitedVertices[i] = 1;
				//		vertexRowPosition[i] = 0;
						visitedPaths[currVertex][i] = 1;
						st.push(i);
						remove = false;
						break;
					}

				}

			}


			if (remove && currVertex != startVertex)
			{
				st.pop();
				//vertexRowPosition[currVertex] = -1;
				visitedVertices[currVertex] = 0;
				for (int s = 0; s < visitedPaths[currVertex].size(); s++)
				{
					visitedPaths[currVertex][s] = 0;
				}

				visitedPaths[st.top()][currVertex] = 1;
			}

			if (currVertex == startVertex && remove)
			{
				break;
			}


			currVertex = st.top();
			remove = true;

			if (currVertex == endVertex)
			{
				stack<int> p;
				st.pop();

				while (currVertex != startVertex)
				{
					p.push(currVertex);
					visitedVertices[currVertex] = 0;
				//	vertexRowPosition[currVertex] = -1;
					for (int i = 0; i < visitedPaths[currVertex].size(); i++)
					{
						visitedPaths[currVertex][i] = 0;
					}

					currVertex = st.top();
					st.pop();

				}
				p.push(currVertex);
				st.push(currVertex);
				//vertexRowPosition[endVertex] = 1;


				paths.push_back(p);

			}

		}


	}

	bool isTraversableFromLeftToRight(int i, int n)
	{
		if (vertex[i].first.compare(vertex[n].first) == 0)
		{
			return false;
		}

		int leftI = 0;
		int leftN = 0;
		int diff;
		int diffChange = 0;
		Connection * con = new Connection;

		for (int s = 0; s < 6; s++)
		{
			leftI += vertex[i].second.order[s];
			leftN += vertex[n].second.order[s];
		}

		diff = leftI - leftN;

		if (diff <= 0 || diff > 2)
		{
			return false;
		}


		for (int s = 6; s < 12; s++)
		{
			if ((vertex[n].second.order[s] > vertex[i].second.order[s] && vertex[i].second.order[s - 6] == 0) ||
				(vertex[i].second.order[s] > vertex[n].second.order[s]))
			{
				return false;
			}

		}


		vector<pair<string, Order>> vertex;
		vertex.push_back(pair<string, Order>(this->vertex[i]));
		vertex.push_back(pair<string, Order>(this->vertex[n]));

		i = 0;
		n = 1;

		for (int s = 0; s < 6; s++)
		{
			while (vertex[i].second.order[s] != vertex[n].second.order[s])
			{
				vertex[i].second.order[s] -= 1;
				diffChange++;
				if (con->firstEmpty())
				{
					con->first = getCharForOrder(s);
				}
				else
				{
					con->second = getCharForOrder(s);
				}

			}

			if (diffChange == diff)
			{
				break;
			}

		}

		if (con->firstEmpty() && con->secondEmpty())
		{
			return false;
		}

		return validateConnection(con);

	}

	bool isTraversableFromRightToLeft(int i, int n)
	{
		if (vertex[i].first.compare(vertex[n].first) == 0)
		{
			return false;
		}

		int rightI = 0;
		int rightN = 0;
		int diffChange = 0;
		int diff;
		Connection con;

		for (int s = 6; s < 12; s++)
		{
			rightI += vertex[i].second.order[s];
			rightN += vertex[n].second.order[s];
		}

		diff = rightI - rightN;

		if (diff <= 0 || diff > 2)
		{
			return false;
		}

		for (int s = 0; s < 6; s++)
		{
			if ((vertex[n].second.order[s] > vertex[i].second.order[s] && vertex[i].second.order[s + 6] == 0) ||
				(vertex[i].second.order[s] > vertex[n].second.order[s]))
			{
				return false;
			}

		}

		vector<pair<string, Order>> vertex;
		vertex.push_back(pair<string, Order>(this->vertex[i]));
		vertex.push_back(pair<string, Order>(this->vertex[n]));

		i = 0;
		n = 1;

		for (int s = 6; s < 12; s++)
		{
			while (vertex[i].second.order[s] != vertex[n].second.order[s])
			{
				vertex[i].second.order[s] -= 1;
				diffChange++;
				if (con.firstEmpty())
				{
					con.first = getCharForOrder(s);
				}
				else
				{
					con.second = getCharForOrder(s);
				}

			}

			if (diffChange == diff)
			{
				break;
			}

		}

		if (con.firstEmpty() && con.secondEmpty())
		{
			return false;
		}

		return validateConnection(con);

	}


	bool validateConnection(Connection * con)
	{
		bool valid = true;

		// Zaglim oblig�ti ir j�b�t kop� ar policistu
		if (con->first == 'Z' || con->second == 'Z')
		{
			if (con->first == 'Z' && con->second != 'P' && !(con->secondEmpty()))
			{
				return false;
			}
			else if (con->second == 'Z' && con->first != 'P' && !(con->firstEmpty()))
			{
				return false;
			}
		}

		// Plost� j�b�t noteikti t�vam vai m�tei vai policistam vai zaglim
		if (((con->first == 'D' || con->first == 'E') && (con->second == 'D' || con->second == 'E')) ||
			((con->first == 'D' || con->first == 'E') && con->secondEmpty()) ||
			((con->second == 'D' || con->second == 'E') && con->firstEmpty()))
		{
			return false;
		}

		

		// T�vs nedr�kst b�t kop� ar meitu
		if ((con->first == 'T' || con->second == 'T') && (con->first == 'E' || con->second == 'E'))
		{
			return false;
		}

		//M�te nedr�kst b�t kop� ar d�lu
		if ((con->first == 'M' || con->second == 'M') && (con->first == 'D' || con->second == 'D'))
		{
			return false;
		}

		return valid;

	}

	bool validateConnection(Connection con)
	{
		bool valid = true;

		// Zaglim oblig�ti ir j�b�t kop� ar policistu VAI vienam pa�am
		if (con.first == 'Z' || con.second == 'Z')
		{
			if (con.first == 'Z' && con.second != 'P' && !(con.secondEmpty()))
			{
				return false;
			}
			else if (con.second == 'Z' && con.first != 'P' && !(con.firstEmpty()))
			{
				return false;
			}
		}

		// Plost� j�b�t noteikti t�vam vai m�tei vai policistam vai zaglim
		if (((con.first == 'D' || con.first == 'E') && (con.second == 'D' || con.second == 'E')) ||
			((con.first == 'D' || con.first == 'E') && con.secondEmpty()) ||
			((con.second == 'D' || con.second == 'E') && con.firstEmpty()))
		{
			return false;
		}

		// T�vs nedr�kst b�t kop� ar meitu
		if ((con.first == 'T' || con.second == 'T') && (con.first == 'E' || con.second == 'E'))
		{
			return false;
		}

		//M�te nedr�kst b�t kop� ar d�lu
		if ((con.first == 'M' || con.second == 'M') && (con.first == 'D' || con.second == 'D'))
		{
			return false;
		}

		return valid;

	}


	char getCharForOrder(int i)
	{
		if (i >= OrderIndx::TAFT)
		{
			i -= 6;
		}

		if (i == OrderIndx::TBEF)
		{
			return 'T';
		}
		else if (i == OrderIndx::MBEF)
		{
			return 'M';
		}
		else if (i == OrderIndx::DBEF)
		{
			return 'D';
		}
		else if (i == OrderIndx::EBEF)
		{
			return 'E';
		}
		else if (i == OrderIndx::PBEF)
		{
			return 'P';
		}
		else if (i == OrderIndx::ZBEF)
		{
			return 'Z';
		}

		return '\0';
	}


	int calcBefDiff(int i, int n)
	{
		int compDiff = 0;
		int diff = 0;

		for (int s = 0; s < 6; s++)
		{
			if (vertex[i].second.order[s] != vertex[n].second.order[s])
			{
				diff = abs(vertex[i].second.order[s] - vertex[n].second.order[s]);
				compDiff += diff;
			}
		}

		return compDiff;
	}

	int calcAftDiff(int i, int n)
	{
		int compDiff = 0;
		int diff = 0;

		for (int s = 6; s < 12; s++)
		{
			if (vertex[i].second.order[s] != vertex[n].second.order[s])
			{
				diff = abs(vertex[i].second.order[s] - vertex[n].second.order[s]);
				compDiff += diff;
			}
		}
		return compDiff;
	}


	bool overused(Used &u)
	{
		if (u.used[UsedIndx::D] > 2 || u.used[UsedIndx::E] > 2)
		{
			return true;
		}
		if (u.used[UsedIndx::T] > 1 || u.used[UsedIndx::M] > 1 || u.used[UsedIndx::P] > 1 ||
			u.used[UsedIndx::Z] > 1 || u.used[UsedIndx::R] > 1)
		{
			return true;
		}

		return false;
	}


	bool checkDupesVal(string &v, Order &o)
	{
		bool valid = validateOrder(o);
		if (!(valid))
		{
			return valid;
		}

		valid = validVertex(v, o);

		if (!(valid))
		{
			return valid;
		}

		for (int i = 0; i < vertex.size(); i++)
		{
			if (vertex[i].first.compare(v) == 0 || o.compare(vertex[i].second))
			{
				return false;
			}
		}

		return true;
	}


	bool validVertex(string &v, Order &o)
	{
		bool riverUsed = false;
		for (int i = 0; i < v.length(); i++)
		{
			if (riverUsed)
			{
				break;
			}

			if (v[i] == '|')
			{
				riverUsed = true;
			}
		}

		if (!(riverUsed))
		{
			return false;
		}


		if (o.order[OrderIndx::TBEF] + o.order[OrderIndx::TAFT] != 1)
		{
			return false;
		}
		if (o.order[OrderIndx::MBEF] + o.order[OrderIndx::MAFT] != 1)
		{
			return false;
		}
		if (o.order[OrderIndx::PBEF] + o.order[OrderIndx::PAFT] != 1)
		{
			return false;
		}
		if (o.order[OrderIndx::ZBEF] + o.order[OrderIndx::ZAFT] != 1)
		{
			return false;
		}
		if (o.order[OrderIndx::DBEF] + o.order[OrderIndx::DAFT] != 2)
		{
			return false;
		}
		if (o.order[OrderIndx::EBEF] + o.order[OrderIndx::EAFT] != 2)
		{
			return false;
		}

		return true;
	}


	bool validateOrder(Order &o)
	{
		// Policistam ir j�b�t kop� ar zagli VAI zaglis ir viens pats
		if ((o.order[OrderIndx::PBEF] == 1 && o.order[OrderIndx::ZAFT] == 1) || (o.order[OrderIndx::PAFT] == 1 && o.order[OrderIndx::ZBEF] == 1))
		{
			if (o.order[OrderIndx::ZAFT] == 1)
			{
				for (int i = 6; i < 12; i++)
				{
					if (o.order[i] > 0 && i != OrderIndx::ZAFT)
					{
						return false;
					}
				}
			}
			else if (o.order[OrderIndx::ZBEF] == 1)
			{
				for (int i = 0; i < 6; i++)
				{
					if (o.order[i] > 0 && i != OrderIndx::ZBEF)
					{
						return false;
					}
				}
			}

		}

		// T�vs nedr�kst b�t kop� ar meit�m bez m�tes
		if ((o.order[OrderIndx::TBEF] == 1 && o.order[OrderIndx::EBEF] > 0 && o.order[OrderIndx::MBEF] == 0) ||
			(o.order[OrderIndx::TAFT] == 1 && o.order[OrderIndx::EAFT] > 0 && o.order[OrderIndx::MAFT] == 0))
		{
			return false;
		}

		// M�te nedr�kst b�t kop� ar d�liem bez t�va
		if ((o.order[OrderIndx::MBEF] == 1 && o.order[OrderIndx::DBEF] > 0 && o.order[OrderIndx::TBEF] == 0) ||
			(o.order[OrderIndx::MAFT] == 1 && o.order[OrderIndx::DAFT] > 0 && o.order[OrderIndx::TAFT] == 0))
		{
			return false;
		}

		return true;
	}



	Order makeOrder(string &v)
	{
		Order o;
		bool afterRiver = false;

		for (int i = 0; i < v.length(); i++)
		{
			if (v[i] == '|')
			{
				afterRiver = true;
				continue;
			}

			if (afterRiver)
			{
				if (v[i] == 'T')
				{
					o.order[OrderIndx::TAFT] += 1;
				}
				else if (v[i] == 'M')
				{
					o.order[OrderIndx::MAFT] += 1;
				}
				else if (v[i] == 'D')
				{
					o.order[OrderIndx::DAFT] += 1;
				}
				else if (v[i] == 'E')
				{
					o.order[OrderIndx::EAFT] += 1;
				}
				else if (v[i] == 'P')
				{
					o.order[OrderIndx::PAFT] += 1;
				}
				else if (v[i] == 'Z')
				{
					o.order[OrderIndx::ZAFT] += 1;
				}
			}
			else
			{
				if (v[i] == 'T')
				{
					o.order[OrderIndx::TBEF] += 1;
				}
				else if (v[i] == 'M')
				{
					o.order[OrderIndx::MBEF] += 1;
				}
				else if (v[i] == 'D')
				{
					o.order[OrderIndx::DBEF] += 1;
				}
				else if (v[i] == 'E')
				{
					o.order[OrderIndx::EBEF] += 1;
				}
				else if (v[i] == 'P')
				{
					o.order[OrderIndx::PBEF] += 1;
				}
				else if (v[i] == 'Z')
				{
					o.order[OrderIndx::ZBEF] += 1;
				}
			}

		}

		return o;
	}

	void printVerices(fstream &f)
	{
		for (int i = 0; i < vertex.size(); i++)
		{
			f << i << " " << vertex[i].first << " ";

			if (edges.size() > i && edges[i].size() > 0)
			{
				f << "Edges to";

				for (int n = 0; n < edges[i].size(); n++)
				{
					f << " (";
					f << edges[i][n]->edgeTo << "[ " << edges[i][n]->first << edges[i][n]->second << " ]";
					f << " )";
				}
			}

			f << "\n";

		}

	}

	void printPaths(fstream &f)
	{
		for (int i = 0; i < paths.size(); i++)
		{
			f << "Path #" << i << "\n";
			while (paths[i].empty() != true)
			{
				f << paths[i].top() << " ";
				paths[i].pop();
			}
			f << "\n \n";
		}
	}


};

int main()
{
	string arr[] = { "T", "M", "D", "D", "E", "E", "P", "Z", "|"};
	Used u;
	VertexGenerator v(arr, 9);

	int i;


	v.generatePermutations("", u);
	v.dfs2();

	fstream f;
	f.open("vertex.txt", ios::out);
	v.printVerices(f);
	v.printPaths(f);
	f.close();
	

	return 0;
}